# Bitbucket Pipelines Pipe: LFTP Deploy

Deploy files to a remote server using LFTP and a batch-file.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
    
```yaml
- pipe: usmedia/lftp-deploy:0.1.0
  variables:
    USER: '<string>'
    SERVER: '<string>'
    REMOTE_PATH: '<string>'
    LOCAL_PATH: '<string>'
    # BATCH_FILE: '<string>' # Optional.
    # PASS: '<string>' # Optional.
    # SSH_KEY: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable              | Usage                 |
| --------------------- | --------------------- |
| SERVER (*)            | The remote host to transfer the files to. |
| USER (*)              | The user on the remote host to connect as. |
| REMOTE_PATH (*)       | The remote path to deploy files to. |
| LOCAL_PATH (*)        | The local path to folder containing files to be deployed. LOCAL_PATH may contain glob characters and may match multiple files. |
| BATCH_FILE            | An alternate batch file to use with lftp, usefull for creating your own batching file (see [LFTP docs](https://lftp.yar.ru/lftp-man.html) for more details). |
| PASS                  | An password which can be used within the batch_file, normally not needed because of an SSH Key. |
| SSH_KEY               | An alternate SSH_KEY to use instead of the key configured in the Bitbucket Pipelines admin screens (which is used by default). This should be encoded as per the instructions given in the docs for [using multiple ssh keys](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-multiple_keys) |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Details
LFTP copies files and directories from your local system to another server. It uses SSL for data transfer, which uses the same authentication
and provides the same security as SSH. It may also use many other features of SSH, such as compression.

More details: [lftp manual page](https://lftp.yar.ru/lftp-man.html)

By default, the pipe will automatically use your configured SSH key and known_hosts file configured from the [Bitbucket Pipelines
administration pages](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html). You can pass
the `SSH_KEY` parameter to use an alternate SSH key as per the instructions in the docs for
[using multiple ssh keys](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-multiple_keys)


## Prerequisites
If you want to use the default behaviour for using the configured SSH key and known hosts file, you must have configured 
the SSH private key and known_hosts to be used for the pipe in your Pipelines settings
(see [docs](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html))

## Batch File Variables
If you are using a custom batch-file, note that you can use certain env-variables (envsubst is used on the file).

All variables given along to the pipe are available, this includes BITBUCKET variables. See the logfile in the pipelines for an up-to-date list.  

### Extra available variables
For ease of use, some extra env variables are made available for use within the batch file.

| Variable       | Description    |
| -------------- | -------------- |
| NOW            | Current timestamp, datetime format: `%Y%m%d-%H%M%S` |
| NOW_DATE       | Current timestamp, date format: `%Y%m%d` |
| NOW_SECONDS    | Current timestamp, seconds format: `%s` |

## Examples

### Basic example:
    
```yaml
script:
  - pipe: usmedia/lftp-deploy:0.1.0
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/html/'
      LOCAL_PATH: 'dist'
```

### Advanced examples:
Here we pass an alternate batch-file to the lftp command and enable extra debugging.

    
```yaml
script:
  - pipe: usmedia/lftp-deploy:0.1.0
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/html/'
      LOCAL_PATH: 'dist/*'
      DEBUG: 'true'
      BATCH_FILE: 'ec-batch'
```

Example with alternate SSH key.
    
```yaml
script:
  - pipe: usmedia/lftp-deploy:0.1.0
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/html/'
      LOCAL_PATH: 'dist'
      SSH_KEY: $MY_SSH_KEY
      DEBUG: 'true'
```

## License
Copyright (c) 2019 Us Media B.V.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
