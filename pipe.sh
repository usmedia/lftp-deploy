#!/bin/bash
# Deploy files using LFTP https://lftp.yar.ru/lftp-man.html
# Bitbucket Pipelines
#
# Required globals:
#   SERVER
#   USER
#   REMOTE_PATH
#   LOCAL_PATH
#
# Optional globals:
#   BATCH_FILE
#   PASS
#   SSH_KEY
#   DEBUG

# Begin Standard 'imports'
set -e
set -o pipefail


grey="\\e[37m"
blue="\\e[36m"
red="\\e[31m"
green="\\e[32m"
reset="\\e[0m"

# Logging, loosely based on http://www.ludovicocaldara.net/dba/bash-tips-4-use-logging-levels/
info() { echo -e "${blue}INFO: $*${reset}"; }
error() { echo -e "${red}ERROR: $*${reset}"; }
debug() { if [[ "${DEBUG}" == "true" ]]; then echo -e "${grey}DEBUG: $*${reset}"; fi }
success() { echo -e "${green}✔ $*${reset}"; }
fail() { echo -e "${red}✖ $*${reset}"; }

# End standard 'imports'

LFTP_DEBUG_ARGS=
## Enable debug mode.
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
    LFTP_DEBUG_ARGS="-d"
  fi
}

validate() {
  # mandatory parameters
  : SERVER=${SERVER:?'SERVER variable missing.'}
  : USER=${USER:?'USER variable missing.'}
  : REMOTE_PATH=${REMOTE_PATH:?'REMOTE_PATH variable missing.'}
  : LOCAL_PATH=${LOCAL_PATH:?'LOCAL_PATH variable missing.'}
}

setup_ssh_dir() {
  INJECTED_SSH_CONFIG_DIR="/opt/atlassian/pipelines/agent/ssh"
  # The default ssh key with open perms readable by alt uids
  IDENTITY_FILE="${INJECTED_SSH_CONFIG_DIR}/id_rsa_tmp"
  # The default known_hosts file
  KNOWN_HOSTS_FILE="${INJECTED_SSH_CONFIG_DIR}/known_hosts"

  mkdir -p ~/.ssh || debug "adding ssh keys to existing ~/.ssh"
  touch ~/.ssh/authorized_keys

  # If given, use SSH_KEY, otherwise check if the default is configured and use it
  if [ "${SSH_KEY}" != "" ]; then
    debug "Using passed SSH_KEY"
    (umask  077 ; echo ${SSH_KEY} | base64 -d > ~/.ssh/pipelines_id)
  elif [ ! -f ${IDENTITY_FILE} ]; then
    error "No default SSH key configured in Pipelines."
    exit 1
  else
    debug "Using default ssh key"
    cp ${IDENTITY_FILE} ~/.ssh/pipelines_id
  fi

  if [ ! -f ${KNOWN_HOSTS_FILE} ]; then
    error "No SSH known_hosts configured in Pipelines."
    exit 2
  fi

  cat ${KNOWN_HOSTS_FILE} >> ~/.ssh/known_hosts
  if [ -f ~/.ssh/config ]; then
    debug "Appending to existing ~/.ssh/config file"
  fi

  echo "IdentityFile ~/.ssh/pipelines_id" >> ~/.ssh/config
  chmod -R go-rwx ~/.ssh/
}

setup_batch_file() {
  export NOW=$(date +"%Y%m%d-%H%M%S")
  export NOW_DATE=$(date +"%Y%m%d")
  export NOW_SECONDS=$(date +"%s")
  BATCH_FILE=${BATCH_FILE:-'/etc/pipe/batch_default'}

  if [[ "${DEBUG}" == "true" ]]; then
    info "Contents of batch file '${BATCH_FILE}' (pre-envsubst)."
    cat ${BATCH_FILE}
  fi

  envsubst < ${BATCH_FILE} > batch-substituted
}

run_pipe() {
  info "Starting LFTP deployment to '${SERVER}:${REMOTE_PATH}'"

  debug lftp ${LFTP_DEBUG_ARGS} -f batch-substituted
  set +e
  lftp ${LFTP_DEBUG_ARGS} -f batch-substituted
  STATUS=$? # status of last command of this pipe, i.e. lftp
  set -e

  if [[ "${STATUS}" == "0" ]]; then
    success "Deployment finished."
  else
    fail "Deployment failed."
  fi

  exit $STATUS
}

validate
enable_debug
setup_ssh_dir
setup_batch_file
run_pipe
